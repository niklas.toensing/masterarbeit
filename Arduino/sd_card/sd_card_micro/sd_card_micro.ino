#include <SD.h>   //SD Card Bibliothek
#include <SPI.h>  //Bibliothek zum zugriff auf die SPI Schnittstelle des Arduinos
File outFile; // Die Datei welche geschrieben bzw. gelesen werden soll
int pinCS = 9; // Chip Select Pin 10 auf dem Arduino Uno
String filename = "test.txt"; //Dateiname der zu lesenden bzw. schreibenden Datei
void setup() {
  Serial.begin(9600); //Serielle Kommunikation mit 9600 Baud beginnen
  pinMode(pinCS, OUTPUT);
  if (SD.begin()){ //Wenn die SD Card initialisiert werden konnte dann....
    Serial.println("SD Karte konnte erfolgreich geladen werden!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
  } else {
    //Dieser Block wird ausgeführt wenn die SD Card nicht initialisiert werden konnte.
    Serial.println("SD Karte konnte NICHT erfolgreich geladen werden!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
    Serial.println("---Programm wird beendet!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
    return; //vorzeitiges beenden der setup Methode
  }
}

void loop() {
  // Leere Methode die gesamte Logik wird im Setup erledigt
}
