/*
 * Programm written with Arduino IDE
 * Reads the drops counted by the dropcounter all the time
 * All 30 seconds the spectrum of the sample is measured and the
 * spectrum together with the drop count and a timestamp is saved
 * to a file on the SD card.
 * Delay times can be changed in the loop part 
 * function readSpectrometer from Pure Engineering LLC.
 * Arduino C12880MA Example, 2016. 
 * URLhttps://github.com/groupgets/c12880ma.
 * Rest written by Niklas Toensing
 * June 2021
 */
#include <SD.h>   //SD Card library
#include <SPI.h>  //library for spi connection to arduino
#include <TimeLib.h> //library for time functions
#include <DS1307RTC.h> //library to read rtc
#include <Wire.h>

 /*
 * Macro Definitions
 */
#define SPEC_TRG         5
#define SPEC_ST          4
#define SPEC_CLK         3
#define SPEC_VIDEO       A3
#define WHITE_LED        6
#include <elapsedMillis.h>

#define SPEC_CHANNELS    120 // New Spec Channel (total channels 288)
uint16_t data[SPEC_CHANNELS];

File outFile; // file to read write
int pinCS = 10; // Chip Select Pin 10 on Arduino Uno for SD card
char *filename; //pointer to first character of the string with filename


int secs = 1;
int mins = 1;
//drop count
int counter = 0;
int timememo = 0;
//sensormemory
int sensormemo = 0;
//writing memory
int writememo = 0;

tmElements_t tm;
char timest[15]; //14 digits plus the null char

void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low
  // initialize the spec clock with fastest period
//    spec.setClockPeriod(MIN_CLOCK_PULSE_US);
  Serial.begin(115200); // Baud Rate set to 115200
  pinMode(pinCS, OUTPUT);
  if (SD.begin()) { //initialize sd card
    Serial.println("SD true"); //output on serial
  } else {
    Serial.println("SD false"); //output on serial
    Serial.println("break"); //output on serial
    return; //end setup here
  }
  filename = "data.txt";
  // Check to see if the file exists:
  if (SD.exists(filename)) {
    Serial.println("data.txt exists.");
    
  } else {
    Serial.println("data.txt doesn't exist.");
    outFile = SD.open(filename, FILE_WRITE); //open or create file in write mode
    outFile.print("ind");
    outFile.print(";");
    outFile.print("val");
    outFile.print(";");
    outFile.print("datetime");
    outFile.print(";");
    outFile.print("counter");
    outFile.print(";");
    outFile.print("delay");    
    outFile.print("\n");
    outFile.close();
  }
}

/*
 * This functions reads spectrometer data from SPEC_VIDEO
 * Look at the Timing Chart in the Datasheet for more info
 */
void readSpectrometer(){
  int delayTime = 1; // delay time
  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  
 delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  digitalWrite(SPEC_ST, HIGH);
     delayMicroseconds(delayTime);
  //Sample for a period of time
  for(int i = 0; i < 15; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
      
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
     delayMicroseconds(delayTime);
  //Read from SPEC_VIDEO
  for(int i = 0; i < SPEC_CHANNELS; i++){
      data[i] = analogRead(SPEC_VIDEO);
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
        
  }
 
  //Set SPEC_ST to high
  digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 6; i++){
    
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
    
  }

  //Set SPEC_ST to low
  //digitalWrite(SPEC_ST, LOW);
  
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  
}

char *getTimestamp(){ 
  tmElements_t tm;
  if (RTC.read(tm)) { //read current time
     sprintf(timest, "%02d%02d%4d%02d%02d%02d", tm.Day, tm.Month, tmYearToCalendar(tm.Year), tm.Hour, tm.Minute, tm.Second);
     secs = tm.Second;
     mins = tm.Minute;
  }
  return timest;
}

void loop(){
  // read the input on analog pin 1:
  int sensorValue = analogRead(A1);
  //Serial.println(sensorValue);
  //Serial.println(counter);
  // check if sensorValue is > 700 (drop) check if sensorValue is still >700 (sensormemo), set buttontime:
  if (sensorValue > 700 and sensormemo == 0) {
    counter++;
    sensormemo = 1;
    }

  //check if sensorValue is < 700, check if sensorValue was > 700 before (sensormemo), increase counter:
  if (sensorValue < 700 and sensormemo == 1) {
    sensormemo = 0;
  }
  
getTimestamp();
Serial.print(mins);
Serial.print(":");
Serial.print(secs);
Serial.print("\n");
delay(1); //important otherwise code crushes
//read spectrometer all 30s
  if (secs%30 == 0 and !timememo) {
   //Serial.println(secs);
   outFile = SD.open(filename, FILE_WRITE);//open or create file in write mode
   outFile.seek(EOF); //go to end of file
   if (outFile) {
    //Serial.println("read spectrometer");
    analogWrite(WHITE_LED,0); //turn on LED
    readSpectrometer();
    delay(100); //set delay time here
    readSpectrometer();
    for (int i = 0; i < SPEC_CHANNELS; i++){
      outFile.print(i);
      outFile.print(";");
      outFile.print(data[i]);
      //Serial.println(data[i]);
      outFile.print(";");
      outFile.print(timest);
      outFile.print(";");
      outFile.print(counter);
      outFile.print(";");
      outFile.print("100");//write delay time
      outFile.print("\n");              
    }
    
    readSpectrometer();
    delay(50); //set delay time here
    readSpectrometer();
    for (int i = 0; i < SPEC_CHANNELS; i++){
      outFile.print(i);
      outFile.print(";");
      outFile.print(data[i]);
      //Serial.println(data[i]);
      outFile.print(";");      
      outFile.print(timest);
      outFile.print(";");
      outFile.print(counter);
      outFile.print(";");
      outFile.print("50");//write delay time
      outFile.print("\n");              
    }
    outFile.close();
    timememo = 1;
   }
   else {
    Serial.println("Error opening data.txt");
  }
    delay(1);
    analogWrite(WHITE_LED,255);//turn LED off
  }
  //reset writememo after a second
  if (secs%30 !=0 and timememo){
    //Serial.println(secs);
    timememo = 0;
  }
}
