#include <SD.h>   //SD Card library
#include <SPI.h>  //library for spi connection to arduino
#include <TimeLib.h> //library for time functions
#include <DS1307RTC.h> //library to read rtc

File outFile; // file to read write
int pinCS = 10; // Chip Select Pin 10 on Arduino Uno
char *filename; //pointer to first character of the string with filename

// constants won't change. They're used here to set pin numbers:
const int ledPin =  13;      // the number of the LED pin
const long interval1 = 500;           // interval at which to wait (milliseconds)
const long interval2 = 2000;           // interval at which to blink (milliseconds)

// variables will change:
int ledState = LOW;             // ledState used to set the LED
//drop count
int counter = 0;
//buttonmemory
int buttonmemo = 0;
//writing memory
int writememo = 0;

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long buttontime = 0;        // will store last time LED was updated

const char *monthName[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
tmElements_t tm;
char timest[15]; //14 digits plus the null char

void setup() {
  Serial.begin(9600); //begin serial communication with baud 9600
  pinMode(pinCS, OUTPUT);
  if (SD.begin()) { //initialize sd card
    Serial.println("SD card read succesfully!"); //output on serial
  } else {
    Serial.println("SD card was NOT loaded succesfully"); //output on serial
    Serial.println("---programm finishes here!"); //output on serial
    return; //end setup here
  }
  filename = "data.txt";
  // Check to see if the file exists:
  if (SD.exists(filename)) {
    Serial.println("data.txt exists.");
  } else {
    Serial.println("data.txt doesn't exist.");
  }
  //writeContent();
  readContent();
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
}

void readContent() {
  outFile = SD.open("data.txt"); //open or create file in read-only mode
  if (outFile) {
    Serial.println("Read data:"); //output on serial
    while (outFile.available()) {
      Serial.write(outFile.read()); //output of text on serial
    }
    outFile.close();
  } else {
    Serial.println("Error opening data.txt");
  }
}

char *getTimestamp(){ 
  tmElements_t tm;
  if (RTC.read(tm)) { //read current time
     sprintf(timest, "%02d%02d%4d%02d%02d%02d", tm.Day, tm.Month, tmYearToCalendar(tm.Year), tm.Hour, tm.Minute, tm.Second);
  }
  return timest;
}

void writeContent() {
  Serial.println("");
  Serial.println("write content");
  outFile = SD.open(filename, FILE_WRITE); //open or create file in write mode
  if (outFile) {
    outFile.print(getTimestamp()); //write timestamp to file
    outFile.print(";");
    outFile.print(counter); //write count to file
    outFile.println("");
    outFile.close();
  }
  else {
    Serial.println("Error opening test.txt");
  }
}

void loop() {
  unsigned long currentMillis = millis(); //time at this moment
  unsigned long firstMillis;
  unsigned long secondMillis;
  unsigned long writetime;
  // read the input on analog pin 1:
  int sensorValue = analogRead(A1);
  //second and sensor value to serial to check dropcounter
  RTC.read(tm);
  //Serial.print(tm.Second);
  //Serial.print(" ; ");
  //Serial.print(sensorValue);
  //Serial.print(" ; ");
  //Serial.println(counter);
  // check if sensorValue is > 400 (drop) check if sensorValue is still >400 (buttonmemo), set buttontime:
  if (sensorValue > 400 and buttonmemo == 0) {
    //timeStamp = getTimestamp
    //Serial.println(getTimestamp());
    Serial.println("high");
    Serial.println(counter);
    buttontime = currentMillis;
    buttonmemo = 1;
    counter++;
  }
  //check if sensorValue is < 400, check if sensorValue was > 400 before (buttonmemo), increase counter:
  if (sensorValue < 400 and buttonmemo == 1) {
    buttonmemo = 0;
    Serial.println("low");
  }
  //write once a minute to sd, calculate time for writing, reset counter, set writememo high
  if (tm.Second == 20 and !writememo) {
    firstMillis = millis();
    writeContent();
    //Serial.println(getTimestamp());
    secondMillis = millis();
    writetime = secondMillis - firstMillis;
    Serial.println(writetime);
    counter = 0;
    writememo = 1;
  }

  //reset writememo after a second
  if (tm.Second == 21 and writememo) {
    writememo = 0;
  }

  if (currentMillis - buttontime >= interval1 and buttontime > 0) {
    // turn LED on:
    digitalWrite(ledPin, HIGH);

  }
  if (currentMillis - buttontime >= interval1 + interval2 and buttontime > 0) {
    // turn LED off:
    digitalWrite(ledPin, LOW);
  }
}
