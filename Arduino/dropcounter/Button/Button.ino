/*
  Button

  Turns on and off a light emitting diode(LED) connected to digital pin 13,
  when pressing a pushbutton attached to pin 2.

  The circuit:
  - LED attached from pin 13 to ground
  - pushbutton attached to pin 2 from +5V
  - 10K resistor attached to pin 2 from ground

  - Note: on most Arduinos there is already an LED on the board
    attached to pin 13.

  created 2005
  by DojoDave <http://www.0j0.org>
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Button
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 2;     // the number of the pushbutton pin
const int ledPin =  13;      // the number of the LED pin
const long interval1 = 500;           // interval at which to wait (milliseconds)
const long interval2 = 2000;           // interval at which to blink (milliseconds)

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

int ledState = LOW;             // ledState used to set the LED
int counter = 0;
int button = 0;
// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated
unsigned long buttontime = 0;        // will store last time LED was updated

// constants won't change:
void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT);
   // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
unsigned long currentMillis = millis();

 Serial.println(counter); 
  // check if the button is pressed(buttonState) check if button is still pressed (button), set buttontime:
if (buttonState == 1 and button == 0) {
    buttontime = currentMillis;
    button = 1;
  }
  //check if button is not pressed, check if button was pressed before (button), increase counter:
 if (buttonState == 0 and button == 1) {
    button = 0;
    counter++;
  }
 if (currentMillis - buttontime >= interval1 and buttontime > 0){
    // turn LED on:
    digitalWrite(ledPin, HIGH);

 }
 if (currentMillis - buttontime >= interval1+interval2 and buttontime > 0){
    // turn LED off:
    digitalWrite(ledPin, LOW);
 }
  }
