#include <SD.h>   //SD Card Bibliothek
#include <SPI.h>  //Bibliothek zum zugriff auf die SPI Schnittstelle des Arduinos
File outFile; // Die Datei welche geschrieben bzw. gelesen werden soll
int pinCS = 10; // Chip Select Pin 10 auf dem Arduino Uno
char *filename;

void setup() {
  Serial.begin(9600); //Serielle Kommunikation mit 9600 Baud beginnen
  pinMode(10, OUTPUT);
  if (SD.begin(pinCS)){ //Wenn die SD Card initialisiert werden konnte dann....
    Serial.println("SD Karte konnte erfolgreich geladen werden!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
  } else {
    //Dieser Block wird ausgeführt wenn die SD Card nicht initialisiert werden konnte.
    Serial.println("SD Karte konnte NICHT erfolgreich geladen werden!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
    Serial.println("---Programm wird beendet!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
    return; //vorzeitiges beenden der setup Methode
  }
  
  filename = "data.txt"; //Dateiname der zu lesenden bzw. schreibenden Datei
  //writeContent();
  readContent();
  //delay(10);
}
void writeContent(){
  outFile = SD.open(filename, FILE_WRITE); //Öffnet bzw. erzeugt die Datei im Modus schreibend
  if (outFile) { //Wenn die Datei existiert dann...
    Serial.println("Schreiben von Daten..."); //Ausgabe des Textes auf der Seriellen Schnittstelle
    outFile.println("Hallo Welt!"); //Schreiben der Zeile in die Textdatei
    outFile.close(); //Schließen der Datei (Dieses ist wichtig da sonst beim beenden des Sketches dies Daten verloren gehen können.)
    Serial.println("Fertig!"); //Ausgabe des Textes auf der Seriellen Schnittstelle
  } else {
    //Dieser Block wird ausgeführt wenn die Datei nicht erzeugt werden konnte.
    Serial.println("Fehler beim öffnen der Datei test.txt"); //Ausgabe des Textes auf der Seriellen Schnittstelle
  }
}
void readContent(){
  outFile = SD.open(filename); //Öffnet bzw. erzeugt die Datei (im nur lese Modus)
  if (outFile) { //Wenn die Datei existiert dann...
    Serial.println("Lese Daten:"); //Ausgabe des Textes auf der Seriellen Schnittstelle
    // Solange Text in der Datei enthalten ist...
    while (outFile.available()) {
      Serial.write(outFile.read()); //Ausgabe des Textes auf der Seriellen Schnittstelle
    }
    outFile.close();//Schließen der Datei
  } else {
    //Dieser Block wird ausgeführt wenn die Datei nicht gelesen werden konnte.
    Serial.println("Fehler beim öffnen der Datei data.txt"); //Ausgabe des Textes auf der Seriellen Schnittstelle
  }
}
void loop() {
  // Leere Methode die gesamte Logik wird im Setup erledigt
}
