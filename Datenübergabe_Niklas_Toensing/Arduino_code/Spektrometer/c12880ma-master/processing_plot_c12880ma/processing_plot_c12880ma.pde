import processing.serial.*;
import java.io.FileWriter;


Serial myPort; 
String val;
String time;
String counter;
String delay;
int[] data; 
double[] summed;
int draw_sum=0;
int max = 1032;
PrintWriter output = null;

void plotdata()
{
  double summed_max = (max)/875;

  background(0); 

  if (draw_sum !=0)
  {
    for (int i=0; i<summed.length-1; i++)
    {
      line(i, 0, i, 875-(int)(summed[i]/summed_max));
    }
  } else
  {

    for (int i=0; i<data.length-1; i++)
    {
      line(i, 0, i, 875-data[i]);
    }
  }
  
  stroke(255);
}


void setup() 
{
  // Create a new file in the sketch directory
  output = createWriter("../../../../data/test.txt"); 
  output.println("ind;val;datetime;counter;delay");
  println(Serial.list());
  String portName = Serial.list()[1]; //This is the index into the serial list, if you only have one serial device the index is 0
  myPort = new Serial(this, portName, 115200);
  summed = new double[288];

  for (int i = 0; i < 150; i++) 
  {
    summed[i] = 0;
  }
  size(150, 720);
}

void draw()
{
  if ( myPort.available() > 0) 
  {  
    val = myPort.readStringUntil('\n');         // read it and store it in val
    if (val != null)
    {
      time = split(val, ';')[0];
      //String time1 = time[0];
      counter = split(val, ';')[1];
      delay = split(val, ';')[2];      
      data = int(split(val, ',')); 
      print(time);
      print('\n');
      print(counter);
      for (int i = 0; i < data.length; i++) 
      {
        if (i<summed.length)
        {
          summed[i] +=  data[i];
        }
       
        output.print(i);
        output.print(";");
        output.print(data[i]);
        output.print(";");
        output.print(time);
        output.print(";");
        output.print(counter);
        output.print(";");
        output.print(delay);
        output.print("\n");
        //print(data[i]);
        print(' ');
      }
      println( ' ');
      plotdata();
    }
  }
}

void keyPressed() {

  if (key == 'c' ) 
  {
    for (int i = 0; i < summed.length; i++) 
    {
      summed[i] = 0;
    }
  } 
  else if (key == 't' ) 
  {
    if(draw_sum==1)
    {
      draw_sum = 0;
    }
    else
    {
      draw_sum = 1;
    }
  } 
  else if (key == 's')
  {
      output.flush();
      output.close();
      exit();
  }
  else 
  {
  }
}
