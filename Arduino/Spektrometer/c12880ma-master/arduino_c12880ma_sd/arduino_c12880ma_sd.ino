#include <SD.h>   //SD Card library
#include <SPI.h>  //library for spi connection to arduino
#include <TimeLib.h> //library for time functions
#include <DS1307RTC.h> //library to read rtc

/*
 * Macro Definitions
 */
#define SPEC_TRG         A0
#define SPEC_ST          4
#define SPEC_CLK         3
#define SPEC_VIDEO       A3
//#define WHITE_LED        A2
//#define LASER_404        A5

#define SPEC_CHANNELS    100 // New Spec Channel
uint16_t data[SPEC_CHANNELS];

File outFile; // file to read write
int pinCS = 10; // Chip Select Pin 10 on Arduino Uno
//char *filename; //pointer to first character of the string with filename

//const char *monthName[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
tmElements_t tm;

//Put this declaration at the top of your program outside every function so it's global
char timest[15]; //19 digits plus the null char
char tmes[7]; 
char filename[12];

const int buttonPin = 2;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status
//buttonmemory
int buttonmemo = 0;

void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  //pinMode(LASER_404, OUTPUT);
  //pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low
  // initialize the spec clock with fastest period
//    spec.setClockPeriod(MIN_CLOCK_PULSE_US);
  Serial.begin(115200); // Baud Rate set to 115200
  
  pinMode(pinCS, OUTPUT);
  
  if (SD.begin()){ //initialize sd card
    //Serial.println("SD1"); //output on serial
  } else {
    //Serial.println("SD0"); //output on serial
    //Serial.println("---programm finishes here!"); //output on serial
    return; //end setup here
  }
}

/*
 * This functions reads spectrometer data from SPEC_VIDEO
 * Look at the Timing Chart in the Datasheet for more info
 */
void readSpectrometer(){

  int delayTime = 1; // delay time

  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  digitalWrite(SPEC_ST, HIGH);
  delayMicroseconds(delayTime);

  //Sample for a period of time
  for(int i = 0; i < 15; i++){

      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime); 
 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){

      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime); 
      
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  delayMicroseconds(delayTime);

  //Read from SPEC_VIDEO
  for(int i = 0; i < SPEC_CHANNELS; i++){

      data[i] = analogRead(SPEC_VIDEO);
      
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime);
        
  }

  //Set SPEC_ST to high
  digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 6; i++){
    
      digitalWrite(SPEC_CLK, HIGH);
      delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
      delayMicroseconds(delayTime);
    
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);
  
  digitalWrite(SPEC_CLK, HIGH);
  delayMicroseconds(delayTime);
  
}
/*
 * The function below saves the data to a file on SD
 */
void saveData(char *fname){
  //Serial.println("write");
  Serial.println(fname);
  outFile = SD.open(fname, FILE_WRITE); //open or create file in write mode
  if (outFile) {
    Serial.println("f1");
    outFile.println(getTimestamp()); //write timestamp to file
    outFile.print("ind");
    outFile.print(";");
    outFile.print("val");
    outFile.print("\n");
    for (int i = 0; i < SPEC_CHANNELS; i++){
      outFile.print(i);
      outFile.print(";");
      outFile.print(data[i]);
      outFile.print("\n");  
    }
    outFile.close();
  }
  else{
    Serial.println("f0");
    //Serial.print(outFile);
    //Serial.print("\n");
  }
}

char *getTimestamp(){ 
  tmElements_t tm;
  //timest = "-------error-------"; //note if time can't be read
  if (RTC.read(tm)) { //read current time
     sprintf(timest, "%02d%02d%4d%02d%02d%02d", tm.Day, tm.Month, tmYearToCalendar(tm.Year), tm.Hour, tm.Minute, tm.Second);
  }
  //delay(1000);
  //Serial.println(timest);
  return timest;
}

char *getTime(){ 
  tmElements_t tm;
  if (RTC.read(tm)) { //read current time
     sprintf(tmes, "%02d%02d%02d", tm.Hour, tm.Minute, tm.Second);
  }
  //delay(1000);
  //Serial.println(timest);
  return tmes;
}

void loop(){
  buttonState = digitalRead(buttonPin);
  if (buttonState == 1 and buttonmemo == 0) {
    buttonmemo = 1;
    //Serial.println("b1");
  }
  if (buttonState == 0 and buttonmemo == 1){
    //Serial.println("reads");
    getTimestamp();
    getTime();
    //Serial.println(timest);
    sprintf(filename, "d%s.txt", tmes);
    Serial.println(tmes);
    readSpectrometer();
    saveData(filename);
    //printData();
    buttonmemo = 0;
    delay(10);  
  }
}
