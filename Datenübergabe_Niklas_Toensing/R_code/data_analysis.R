#--------------------------------
#plot spectrum from file created with hamamatsu micro-spectrometer
#for master's thesis
#Niklas T�nsing
#Jun 2021
#-------------------------------------------

#Directories
mainpath <- "C:/Users/User/Documents/Masterarbeit/masterarbeit/"
datapath <- paste0(mainpath,"data/")
codepath <- paste0(mainpath, "code/")
calibpath <- paste0(datapath, "Durchlauf/")
#probe1path <- paste0(calibpath, "Probe1/")
#probe2path <- paste0(calibpath, "Probe2/")
probe3path <- paste0(calibpath)

#-------------------------------------------

#packages
library(stats)
library(tidyverse)

library(readxl)
library(lubridate)
library(tibbletime)
library(dplyr)
library(tidyr)
library(ggplot2)
library(hrbrthemes)
library(colorspace)
library(data.table)
library(purrr)
library(dygraphs)
library(xts)

library(lamW)

setwd(mainpath)
#functions

#calibrate pixel to wavelength
calib <- function(pix){
  a0 = 3.083536887E2
  b1 = 2.712914405
  b2 = -1.259118844E-3
  b3 = -7.122532621E-6
  b4 = 7.445401152E-9
  b5 = 7.777621893E-12
  return (a0+b1*pix+b2*pix^2+b3*pix^3+b4*pix^4+b5*pix^5)
}

#DOC concentrations in mg/l from TOC Analyser

doc_samp1 <- 86.00
doc_samp2 <- 103.13
doc_samp3 <- 63.77

#drop volume

#rainfall per drop in l/m^2
rfpd <- 0.017
#raingauge diameter in cm
rgdm <- 9.31
rgr <- rgdm/2
#raingauge surface in cm^2
rga <- pi * rgr^2
#volume der drop in ml
dropvol <- rfpd*rga/10

#read file from spectrometer
read_data <- function(filename){
  return (read_delim(filename, delim = ";", col_types = c(col_double(), col_double(), col_character())))
}

#lambert W function without delay dependence
fit_func <- function(x,alpha, beta){alpha*lambertW0(beta*x)}
#linear function
fit_func1 <- function(x, alpha, beta){alpha*x + beta}
#quadratic function
fit_func_delay <- function(x,a,b,c){a*x^2+b*x+c}
#linear function
fit_func_del <- function(x,a,b){a*x+b}
#lambert W function with delay dependence two parametric
fit_func_xD <- function(x,d,n,m){n*lambertW0(m*x/d)}
#lambert W function with delay dependence three parametric
fit_func_xD1 <- function(x,d,n,m,p){n*lambertW0(m*x/(d+p))}
# f(c) = c * exp(c)
fit_func_conc <- function(c,d,a,b,alpha){(a*d+b)*c*exp(alpha*c)}
#-----------------------------
#main
#filenames
calibmainnames <- list.files(path = calibpath, pattern = "*50.TXT")
#name without path
calibmainnamesl <- paste0(calibpath, calibmainnames)

#probe1names <- list.files(path = probe1path, pattern = "*.txt")
#name without path
#probe1namesl <- paste0(probe1path, probe1names)

#probe2names <- list.files(path = probe2path, pattern = "*.txt")
#name without path
#probe2namesl <- paste0(probe2path, probe2names)

probe3names <- list.files(path = probe3path, pattern = "*10_25_50_time.TXT")
#name without path
probe3namesl <- paste0(probe3path, probe3names)

#----------------------------------------------------------------
#read files calib main
calibmain_data_frame_list <- lapply(calibmainnamesl, function(calibmainnamesl)
  read_delim(calibmainnamesl, delim = ";", col_types = c(col_double(), col_double(), col_character(), col_double(), col_double())))

#convert pix into wavelength
calibmain_data_frame_list <-lapply(calibmain_data_frame_list, function(calibmain_data_frame_list)
  cbind(calibmain_data_frame_list, wavelength = calib(calibmain_data_frame_list$ind)))

#change datetime type to character
calibmain_data_frame_list <-lapply(calibmain_data_frame_list, function(calibmain_data_frame_list)
  transform(calibmain_data_frame_list, datetime =as.character(datetime)))


#write delay and concentration and sample into coloumn

calibmain_sample <- str_split(calibmainnames, pattern = "_",simplify = T)[,1]

for(i in 1:length(calibmain_data_frame_list)){
  calibmain_data_frame_list[[i]]$sample <- rep(calibmain_sample[[i]],nrow(calibmain_data_frame_list[[1]]))
}


#--------------------------------------------------
#read files probe1
#probe1_data_frame_list <- lapply(probe1namesl, function(probe1namesl)
 # read_delim(probe1namesl, delim = ";", col_types = c(col_double(), col_double(), col_character())))

#convert pix into wavelength
#probe1_data_frame_list <-lapply(probe1_data_frame_list, function(probe1_data_frame_list)
 # cbind(probe1_data_frame_list, wavelength = calib(probe1_data_frame_list$ind)))

#change datetime type to character
#probe1_data_frame_list <-lapply(probe1_data_frame_list, function(probe1_data_frame_list)
 # transform(probe1_data_frame_list, datetime =as.character(datetime)))

#average data from one file
#probe1_avg_list <- map(probe1_data_frame_list, ~.x %>%
 #                      group_by(wavelength) %>%
  #                     summarise_at(vars(val), funs(mean(., na.rm=TRUE))))
#
#write delay and concentration into coloumn

#probe1_delays <- str_split(probe1names, pattern = "_",simplify = T)[,2]
#probe1_delays <- str_split(probe1_delays, pattern = "delay",simplify = T)[,2]
#probe1_conc <- str_split(probe1names, pattern = "_",simplify = T)[,1]
#probe1_conc <- str_split(probe1_conc, pattern = "prozent",simplify = T)[,1]

#for(i in 1:length(probe1_avg_list)){
#  probe1_avg_list[[i]]$delay <- rep(probe1_delays[[i]],nrow(probe1_avg_list[[1]]))
#  probe1_avg_list[[i]]$conc <- rep(probe1_conc[[i]],nrow(probe1_avg_list[[1]]))
#  probe1_avg_list[[i]]$sample <- "sample1"
#}

#merge list into one dataframe
#probe1_avg <- bind_rows(probe1_avg_list)

#multiplicate with absolute doc concentration
#probe1_avg$doc <- as.numeric(probe1_avg$conc)/100*doc_samp1

#-------------------------------------------------------------
#read files probe2
#probe2_data_frame_list <- lapply(probe2namesl, function(probe2namesl)
 # read_delim(probe2namesl, delim = ";", col_types = c(col_double(), col_double(), col_character())))

#convert pix into wavelength
#probe2_data_frame_list <-lapply(probe2_data_frame_list, function(probe2_data_frame_list)
 # cbind(probe2_data_frame_list, wavelength = calib(probe2_data_frame_list$ind)))

#change datetime type to character
#probe2_data_frame_list <-lapply(probe2_data_frame_list, function(probe2_data_frame_list)
 # transform(probe2_data_frame_list, datetime =as.character(datetime)))

#average data from one file
#probe2_avg_list <- map(probe2_data_frame_list, ~.x %>%
 #                      group_by(wavelength) %>%
  #                     summarise_at(vars(val), funs(mean(., na.rm=TRUE))))

#write delay and concentration into coloumn

#probe2_delays <- str_split(probe2names, pattern = "_",simplify = T)[,2]
#probe2_delays <- str_split(probe2_delays, pattern = "delay",simplify = T)[,2]
#probe2_conc <- str_split(probe2names, pattern = "_",simplify = T)[,1]
#probe2_conc <- str_split(probe2_conc, pattern = "prozent",simplify = T)[,1]

#for(i in 1:length(probe2_avg_list)){
 # probe2_avg_list[[i]]$delay <- rep(probe2_delays[[i]],nrow(probe2_avg_list[[1]]))
  #probe2_avg_list[[i]]$conc <- rep(probe2_conc[[i]],nrow(probe2_avg_list[[1]]))
  #probe2_avg_list[[i]]$sample <- "sample2"
#}


#-------------------------------------------------------------------------------
#read files probe3
probe3_data_frame_list <- lapply(probe3namesl, function(probe3namesl)
  read_delim(probe3namesl, delim = ";", col_types = c(col_double(), col_double(), col_character(), col_double(),col_double())))

#convert pix into wavelength
probe3_data_frame_list <-lapply(probe3_data_frame_list, function(probe3_data_frame_list)
  cbind(probe3_data_frame_list, wavelength = calib(probe3_data_frame_list$ind)))

#change datetime type to character
probe3_data_frame_list <-lapply(probe3_data_frame_list, function(probe3_data_frame_list)
  transform(probe3_data_frame_list, datetime =as.character(datetime)))

#write delay and concentration into coloumn
for(i in 1:length(probe3_data_frame_list)){
  probe3_data_frame_list[[i]]$sample <- "sample3"
}

#----------------------------------------------------
#read fitparameters

fitparametersdoc <- read_delim(paste0(datapath,"Kalibrierung/fitparameters/fitparametersdoc.txt"), delim = ";")
#######################################################
#substract noise

#dataframe$cor_val <- 0

dataframe <- probe3_data_frame_list[[1]]

dataframe_dark<- calibmain_data_frame_list[[1]] %>% slice(3401:4481)


#average dataframe_dark
dataframe_dark <- dataframe_dark %>% 
  group_by(wavelength) %>%
  summarise_at(vars(val), funs(mean(., na.rm=TRUE))) %>% 
  ungroup()

dataframe <- dataframe %>% 
  mutate(cor_val = 0)

dataframe_dark <- dataframe_dark %>% slice(rep(row_number(),nrow(dataframe)/nrow(dataframe_dark)))

dataframe<- dataframe %>% 
  mutate(cor_val = val - dataframe_dark$val) 

#dataframe<- dataframe %>% slice(2641:nrow(dataframe))

data_int <- dataframe %>% 
  filter(wavelength > 440 & wavelength < 522) %>% 
  mutate(delay = as.character(delay)) %>%
  group_by(datetime,counter, delay, sample) %>% 
  summarise(integrate = sum(cor_val)) #%>% 

#data_timestamp <- dataframe %>% 
 # filter(ind == 0)

#data_int$datetime <- data_timestamp$datetime
#head(data_int)

data_int$datetime <- strptime(data_int$datetime,format="%d%m%Y%H%M%S")
data_int$datetime <- as.POSIXct(data_int$datetime)
data_int$volume <- data_int$counter*dropvol

#convert integral into doc concentration

data_int <- data_int %>% 
  group_by(delay) %>% 
  mutate(delay = as.numeric(delay)) %>% 
  mutate(doc = fit_func_xD1(integrate,delay,fitparametersdoc$n[which(fitparametersdoc$sample == "sample3")],
                            fitparametersdoc$m[which(fitparametersdoc$sample == "sample3")],
                            fitparametersdoc$p[which(fitparametersdoc$sample == "sample3")])) %>% 
  ungroup()

##################################
#actual volume
vol <- seq(0,90,9)
vol2 <- rep(vol,4)
vol2 <- sort(vol2)
vol2 <- vol2[1:40]
data_int$act_vol <- vol2

#actual doc concentration
conc <- c(rep(0,8),rep(10,4),rep(25,4),rep(50,8),
          rep(25,4),rep(10,4),rep(0,8))
doc_samp3_list <- conc*doc_samp3/100

data_int$act_doc <- doc_samp3_list
############plotting#####################

plot_colors <- c("#E16A86",
                 "#C18500",
                 "#799D00",
                 "#00AB6E",
                 "#00A9BE",
                 "#6C8EE6")

seq_colors <- c("#FABFAD",
                "#CBA079",
                "#928261",
                "#605F4C",
                "#363932",
                "#040404")

seq_colors2 <- c("#F4B89F",
                 "#B3956E",
                 "#736E55",
                 "#3E4138")

seq_colors3 <- c("#E6B759",
                "#DC8C1B",
                "#C6622A",
                "#9C3E40",
                "#6A1F39",
                "#1D0B14")

seq_colors4 <- c("#DE9529",
                 "#BE5A32",
                 "#7F2B3F",
                 "#1D0B14")

#Create a custom color scale
groundcolors <- c("#E2E6BD",
                  "#EEE286",
                  "#F5D579",
                  "#F5C36E",
                  "#EEAB65",
                  "#E08E5C",
                  "#CB6D53",
                  "#AF4548",
                  "#8E063B")

#concentrations
names(seq_colors) <- c("005", "010", "025", "050", "075", "100")
#delay times
names(seq_colors2) <- c("020", "050", "100", "200")


#Create a custom symbol scale
shapeScale <- scale_shape_manual(name = "sample", values = c(1,3,2))
shapefield <- scale_shape_manual(name = "sample", values = seq(1,18))

#-------------------------------------------------------------------
#plot
#-------------------------------------------------------------------
#spectrum
for (c in data_int$counter[which(data_int$delay == "50")]){
  #dt <- dataframe$datetime[which(dataframe$counter == c & dataframe$ind == 0)]
  dataframe %>% 
  filter(sample == "sample3" & delay == "50" & counter == c) %>% 
ggplot(aes(x = wavelength, y = cor_val))+
  geom_line(size = 1)+
  coord_cartesian(xlim = c(300, 650), ylim = c(0,900)) +
  labs (title = paste("Spectrum of sample 3 with 50 milliseconds delay at",c,"drops"),
        y = "intesity [AU]",
        x = "wavelength [nm]")+
  theme_light()+
  theme(legend.position = c(0.85,0.65))
  plotfile <- paste0("plots/durchlauf/delay50/time/sample3_c",c,".png")
  print(plotfile)
  ggsave(plotfile, width = 15, height = 10, units = "cm")
}

data_int$delay <- as.character(data_int$delay)

data_int<- data_int %>% 
  mutate(delay = replace(delay, delay == "50", "050"))

#peak integrals
#one sample
data_int %>% 
  filter(sample == "sample3") %>% 
  ggplot() + theme_light() + theme(legend.position = c(0.85,0.75))+
  geom_point(aes(x = datetime, y = doc, col = delay),size = 1.5, pch = 3, stroke = 1.5)+
  #geom_point(aes(x = datetime, y = volume), size = 1.5, pch = 1, stroke = 1.5)+
  #geom_point(aes(x = datetime, y = act_vol), size = 1.5, pch = 2, stroke = 1.5)+
  geom_point(aes(x = datetime, y = act_doc), size = 1.5, pch = 3, stroke = 1.5)+
  scale_colour_manual(name = "delay [ms]", values = c("#E3A946", "#AB4A3D"), labels = c("50","100"))+
  coord_cartesian(ylim = c(0,35)) +
  labs (title = "Measured and expected DOC concentration of sample 3 over time\n",
        y = "DOC concentration [mg/l]",
        x = "time")

#all samples
data_int %>% 
  ggplot(aes(x = conc , y = integrate, col = delay, shape = sample)) + theme_light() +
  geom_point(lwd = 2, stroke = 1.5)+
  scale_colour_manual(name = "delay [ms]", values = seq_colors2, labels = c("20","50","100","200"))+
  shapeScale+
  coord_cartesian(xlim = c(0, 100), ylim = c(0,35000)) +
  labs (title = "Integrals of fluorescence peak of all samples",
        y = "integral value [AU]",
        x = "concentration [vol%]")

#make dataframe with int and conc values for fit curve
data_fit_plot <- data.frame(NULL)

x_fit = seq(0, 30000, 100)
for (samp in c("sample1", "sample2", "sample3")){
  for (del in c(20,50,100,200)){
    y_fit <- fit_func(x_fit, data_fit$alpha[which(data_fit$sample == samp & data_fit$delay == del)],
                      data_fit$beta[which(data_fit$sample == samp & data_fit$delay == del)])
    temp <- data.frame(x_fit, y_fit, samp, del)
    data_fit_plot <- rbind(data_fit_plot, temp)
  }
}


#data_int$delay <- as.character(data_int$delay)

#data_int<- data_int %>% 
 # mutate(delay = replace(delay, delay == "20", "020")) %>% 
  #mutate(delay = replace(delay, delay == "50", "050"))

data_fit_plot<- data_fit_plot %>% 
  mutate(del = replace(del, del == "20", "020")) %>% 
  mutate(del = replace(del, del == "50", "050"))

#plot inverse with regression curve
data_int %>% 
  #filter(sample == "sample3") %>% 
  ggplot() +
  geom_point(aes(y = conc , x = integrate, col = delay, shape = sample), lwd = 2, stroke = 1.5)+
  geom_line(data = data_fit_plot, aes(y = y_fit, x = x_fit, col = del, lty = samp), inherit.aes  = F)+
  scale_colour_manual(name = "delay [ms]", values = seq_colors2, labels = c("20","50","100","200"))+
  shapeScale+
  #coord_cartesian(xlim = c(0, 100), ylim = c(0,40000)) +
  labs (title = "Nonlinear regressions to concentrations of all sample with different delays",
        x = "integral value [AU]",
        y = "concentration [vol%]")+
  theme_light() 

data_int$conc<- as.character(data_int$conc)
data_int$delay<- as.numeric(data_int$delay)

data_int<- data_int %>% 
  mutate(conc = replace(conc, conc == "5", "005")) %>% 
  mutate(conc = replace(conc, conc == "10", "010")) %>% 
  mutate(conc = replace(conc, conc == "25", "025")) %>% 
  mutate(conc = replace(conc, conc == "50", "050")) %>% 
  mutate(conc = replace(conc, conc == "75", "075")) 
  
#plot integrate over delay !BUG!
data_int %>% 
  #filter(sample == "sample1") %>% 
  ggplot() +
  geom_point(aes(y = integrate , x = delay, col = conc, shape = sample), lwd = 2, stroke = 1.5)+
  geom_line(data = data_fit_plot, aes(y = x_fit, x = del, lty = samp), inherit.aes  = F)+
  scale_colour_manual(name = "conc [%]", values = seq_colors, labels = c("5","10","25","50", "75","100"))+
  shapeScale+
  coord_cartesian(xlim = c(0, 100), ylim = c(0,40000)) +
  labs (title = "Integral over delay time for sample 1",
        y = "integral value [AU]",
        x = "delay [ms]")+
  theme_light() 
################################################
#make dataframe with int and conc values for fit curve
data_fit_conc_plot <- data.frame(NULL)

c_fit = seq(0, 100, 1)
for (samp in c("sample1", "sample2", "sample3")){
  for (del in c(20,50,100,200)){
    x_fit <- fit_func_conc(c_fit, del, data_fit_conc$a[which(data_fit_conc$sample == samp)],
                      data_fit_conc$b[which(data_fit_conc$sample == samp)],
                      data_fit_conc$alpha[which(data_fit_conc$sample == samp)])
    temp1 <- data.frame(c_fit, x_fit, samp, del)
    data_fit_conc_plot <- rbind(data_fit_conc_plot, temp1)
  }
}

colnames(data_fit_conc_plot) <-  c("c_fit","x_fit", "samp", "del")
data_int$delay <- as.character(data_int$delay)
data_int$conc <- as.numeric(data_int$conc)
data_fit_conc_plot$del <- as.character(data_fit_conc_plot$del)

data_int<- data_int %>% 
 mutate(delay = replace(delay, delay == "20", "020")) %>% 
  mutate(delay = replace(delay, delay == "50", "050"))

data_fit_conc_plot<- data_fit_conc_plot %>% 
  mutate(del = replace(del, del == "20", "020")) %>% 
  mutate(del = replace(del, del == "50", "050"))

#fit integrate~conc
data_int %>% 
  filter(sample == "sample2") %>% 
  ggplot() +
  geom_point(aes(y = integrate , x = conc, col = delay), lwd = 2, stroke = 1.5, pch = 3)+
  geom_line(data = filter(data_fit_conc_plot, samp == "sample2"), aes(x = c_fit, y = x_fit, col = del), inherit.aes  = F)+
  scale_colour_manual(name = "delay [ms]", values = seq_colors2, labels = c("20","50","100","200"))+
  #shapeScale+
  coord_cartesian(xlim = c(0, 100), ylim = c(0,35000)) +
  labs (title = "Integral over conc for sample 2 with regression",
        y = "integral value [AU]",
        x = "conc [%]")+
  theme_light() 

#############################
#make dataframe with int and conc values for fit curve
data_fit_xD_plot <- data.frame(NULL)

x_fit = seq(0, 30000, 100)
for (samp in c("sample1", "sample2", "sample3")){
  for (del in c(20,50,100,200)){
    y_fit <- fit_func_xD1(x_fit, del, data_fit_xD$n[which(data_fit_xD$sample == samp)],
                           data_fit_xD$m[which(data_fit_xD$sample == samp)],
                           data_fit_xD$p[which(data_fit_xD$sample == samp)])
    temp1 <- data.frame(x_fit, y_fit, samp, del)
    data_fit_xD_plot <- rbind(data_fit_xD_plot, temp1)
  }
}

colnames(data_fit_xD_plot) <-  c("x_fit","y_fit", "samp", "del")

data_int$delay <- as.character(data_int$delay)
data_int$conc <- as.numeric(data_int$conc)
data_fit_xD_plot$del <- as.character(data_fit_xD_plot$del)

data_int<- data_int %>% 
  mutate(delay = replace(delay, delay == "20", "020")) %>% 
  mutate(delay = replace(delay, delay == "50", "050"))

data_fit_xD_plot<- data_fit_xD_plot %>% 
  mutate(del = replace(del, del == "20", "020")) %>% 
  mutate(del = replace(del, del == "50", "050"))

#fit conc~integrate&delay
data_int %>% 
  filter(sample == "sample3") %>% 
  ggplot() +
  geom_point(aes(x = integrate , y = conc, col = delay), lwd = 2, stroke = 1.5, pch = 3)+
  geom_line(data = filter(data_fit_xD_plot, samp == "sample3"), aes(x = x_fit, y = y_fit, col = del), inherit.aes  = F)+
  scale_colour_manual(name = "delay [ms]", values = seq_colors2, labels = c("20","50","100","200"))+
  coord_cartesian(ylim = c(0, 100), xlim = c(0,35000)) +
  labs (title = "Concentration over integral for sample 3 with regression",
        x = "integral value [AU]",
        y = "conc [%]")+
  theme_light() 

############################################

#make dataframe with int and doc values for fit curve
data_fit_doc_plot <- data.frame(NULL)

x_fit = seq(0, 32000, 100)
for (samp in c("sample1", "sample2", "sample3")){
  for (del in c(20,50,100,200)){
    y_fit <- fit_func_xD1(x_fit, del, data_fit_xDdoc$n[which(data_fit_xDdoc$sample == samp)],
                          data_fit_xDdoc$m[which(data_fit_xDdoc$sample == samp)],
                          data_fit_xDdoc$p[which(data_fit_xDdoc$sample == samp)])
    temp1 <- data.frame(x_fit, y_fit, samp, del)
    data_fit_doc_plot <- rbind(data_fit_doc_plot, temp1)
  }
}

colnames(data_fit_doc_plot) <-  c("x_fit","y_fit", "samp", "del")

data_int$delay <- as.character(data_int$delay)
data_int$doc <- as.numeric(data_int$doc)
data_fit_doc_plot$del <- as.character(data_fit_doc_plot$del)

data_int<- data_int %>% 
  mutate(delay = replace(delay, delay == "20", "020")) %>% 
  mutate(delay = replace(delay, delay == "50", "050"))

data_fit_doc_plot<- data_fit_doc_plot %>% 
  mutate(del = replace(del, del == "20", "020")) %>% 
  mutate(del = replace(del, del == "50", "050"))

#fit conc~integrate&delay
data_int %>% 
  #filter(sample == "sample1") %>% 
  ggplot() +
  geom_point(aes(x = integrate , y = doc, col = delay, shape = sample), size = 1.5, stroke = 1)+
  geom_line(data = filter(data_fit_doc_plot), aes(x = x_fit, y = y_fit, col = del, lty = samp), inherit.aes  = F, size = 0.5)+
  scale_colour_manual(name = "delay [ms]", values = seq_colors4, labels = c("20","50","100","200"))+
  coord_cartesian(ylim = c(0, 100), xlim = c(0,32000)) +
  shapeScale+
  labs (title = "Concentration over integral for all samples  with regression",
        x = "integral value [AU]",
        y = "DOC concentration [mg/l]")+
  theme_light() 

############################################
dygraph(data_avg5, main = "mean")  %>% 
  dyAxis("y", label = "intensity", independentTicks = T) %>%
  dySeries("cor_val", axis = "y",fillGraph = T, color = "red") %>%
  dyRangeSelector()   %>%   
  dyOptions(stepPlot = T, connectSeparatedPoints = T, 
            fillGraph = T, fillAlpha = 0.2)

for (i in (1:length(data_list))){
number <- i
plot(data_list[[number]]$wavelength, data_list[[number]]$val, main = data_list[[number]]$datetime[1], "l",
     ylab = "intensity", xlab = "wavelength [nm]")
grid()
lambda_led <- data_list[[number]]$wavelength[which(data_list[[number]]$val == max(data_list[[number]]$val))]
print(lambda_led)
}
