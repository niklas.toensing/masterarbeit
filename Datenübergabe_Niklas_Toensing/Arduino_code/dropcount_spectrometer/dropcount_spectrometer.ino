#include <SD.h>   //SD Card library
#include <SPI.h>  //library for spi connection to arduino
#include <TimeLib.h> //library for time functions
#include <DS1307RTC.h> //library to read rtc
#include <Wire.h>

 /*
 * Macro Definitions
 */
#define SPEC_TRG         5
#define SPEC_ST          4
#define SPEC_CLK         3
#define SPEC_VIDEO       A3
#define WHITE_LED        6
#include <elapsedMillis.h>

#define SPEC_CHANNELS    150 // New Spec Channel
uint16_t data[SPEC_CHANNELS];

File outFile; // file to read write
int pinCS = 10; // Chip Select Pin 10 on Arduino Uno for SD card
char *filename; //pointer to first character of the string with filename

//drop count
int counter = 0;
int countermemo = 0;
//sensormemory
int sensormemo = 0;
//writing memory
int writememo = 0;


//const char *monthName[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
tmElements_t tm;
char timest[15]; //19 digits plus the null char

void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low
  // initialize the spec clock with fastest period
//    spec.setClockPeriod(MIN_CLOCK_PULSE_US);
  Serial.begin(115200); // Baud Rate set to 115200
  pinMode(pinCS, OUTPUT);
  if (SD.begin()) { //initialize sd card
    //Serial.println("SD true"); //output on serial
  } else {
    Serial.println("SD false"); //output on serial
    Serial.println("break"); //output on serial
    return; //end setup here
  }
  filename = "data.txt";
  // Check to see if the file exists:
  if (SD.exists(filename)) {
    //Serial.println("data.txt exists.");
  } else {
    Serial.println("data.txt doesn't exist.");
  }
}

void writeContent() {
  Serial.println("");
  Serial.println("write content");
  outFile = SD.open(filename, FILE_WRITE); //open or create file in write mode
  if (outFile) {
    outFile.print(getTimestamp()); //write timestamp to file
    outFile.print(";");
    outFile.print(counter); //write count to file
    outFile.println("");
    outFile.close();
  }
  else {
    Serial.println("Error opening test.txt");
  }
}

/*
 * This functions reads spectrometer data from SPEC_VIDEO
 * Look at the Timing Chart in the Datasheet for more info
 */
void readSpectrometer(){
  int delayTime = 1; // delay time
  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  
 delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  digitalWrite(SPEC_ST, HIGH);
     delayMicroseconds(delayTime);
  //Sample for a period of time
  for(int i = 0; i < 15; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
      
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
     delayMicroseconds(delayTime);
  //Read from SPEC_VIDEO
  for(int i = 0; i < SPEC_CHANNELS; i++){
      data[i] = analogRead(SPEC_VIDEO);
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
        
  }
 
  //Set SPEC_ST to high
  digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 6; i++){
    
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
    
  }

  //Set SPEC_ST to low
  //digitalWrite(SPEC_ST, LOW);
  
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  
}

/*
 * The function below prints out data to the terminal or 
 * processing plot
 */
void printData(){
  
  for (int i = 0; i < SPEC_CHANNELS; i++){
    
    Serial.print(data[i]);
    Serial.print(',');
    
  }
  
  Serial.print("\n");
}

char *getTimestamp(){ 
  tmElements_t tm;
  if (RTC.read(tm)) { //read current time
     sprintf(timest, "%02d%02d%4d%02d%02d%02d", tm.Day, tm.Month, tmYearToCalendar(tm.Year), tm.Hour, tm.Minute, tm.Second);
  }
  return timest;
}

void loop(){
  // read the input on analog pin 1:
  int sensorValue = analogRead(A1);

  // check if sensorValue is > 400 (drop) check if sensorValue is still >400 (sensormemo), set buttontime:
  if (sensorValue > 400 and sensormemo == 0) {
    counter++;
    sensormemo = 1;
    //Serial.println(counter);
  }

  //check if sensorValue is < 400, check if sensorValue was > 400 before (buttonmemo), increase counter:
  if (sensorValue < 400 and sensormemo == 1) {
    sensormemo = 0;
  }

//read spectrometer after 8 drops
  if (counter%8 == 0 and counter != countermemo) {
    countermemo = counter;
   //Serial.println("read spectrometer");
    analogWrite(WHITE_LED,255);
    getTimestamp();
    Serial.print(timest);
    Serial.print(";");
    Serial.print(counter);
    Serial.print(";");
    Serial.print("200");
    Serial.print(";");
    readSpectrometer();
    delay(200);
    readSpectrometer();
    printData();
    
    getTimestamp();
    Serial.print(timest);
    Serial.print(";");
    Serial.print(counter);
    Serial.print(";");
    Serial.print("100");
    Serial.print(";");
    readSpectrometer();
    delay(100);
    readSpectrometer();
    printData();
    delay(10);
    analogWrite(WHITE_LED,0);
  }
}
