#--------------------------------
#plot spectrum from file created with hamamatsu micro-spectrometer
#for master's thesis
#Niklas T�nsing
#Jun 2021
#-------------------------------------------

#Directories
mainpath <- "C:/Users/User/Documents/Masterarbeit/masterarbeit/"
datapath <- paste0(mainpath,"data/")
codepath <- paste0(mainpath, "code/")
#-------------------------------------------

#packages
library(stats)
library(tidyverse)
library(readxl)
library(lubridate)
library(tibbletime)
library(dplyr)
library(tidyr)
library(ggplot2)
library(hrbrthemes)
library(colorspace)
library(data.table)
library(purrr)
library(dygraphs)
library(xts)

setwd(mainpath)
#functions
calib <- function(pix){
  a0 = 3.083536887E2
  b1 = 2.712914405
  b2 = -1.259118844E-3
  b3 = -7.122532621E-6
  b4 = 7.445401152E-9
  b5 = 7.777621893E-12
  return (a0+b1*pix+b2*pix^2+b3*pix^3+b4*pix^4+b5*pix^5)
  }
#-----------------------------
#main

dataname <- "water_delay200_led255_2.txt"

data <- read_delim(paste0(datapath,dataname), delim = ";", col_types = c(col_double(), col_double(), col_character()))

data$wavelength <- calib(data$ind)

#data$datetime <- as.numeric(data$datetime)
data_temp <- data %>% 
  group_by(datetime)
data_avg <- data %>% 
  group_by(wavelength) %>% 
  summarise_at(vars(val), funs(mean(., na.rm=TRUE)))

data_list <- group_split(data_temp)

plot(data_avg$wavelength, data_avg$val, main = "mean", "l",
     ylab = "intensity", xlab = "wavelength [nm]", xlim = c(350,500))
grid(nx=15, ny = 0, col = "grey", lty = "solid")
lambda_led_mean <- data_avg$wavelength[which(data_avg$val == max(data_avg$val))]

dygraph(data_avg, main = "mean")  %>% 
  dyAxis("y", label = "intensity", independentTicks = T) %>%
  dySeries("val", axis = "y",fillGraph = T, color = "red") %>%
  dyRangeSelector()   %>%   
  dyOptions(stepPlot = T, connectSeparatedPoints = T, 
            fillGraph = T, fillAlpha = 0.2)

for (i in (1:length(data_list))){
number <- i
plot(data_list[[number]]$wavelength, data_list[[number]]$val, main = data_list[[number]]$datetime[1], "l",
     ylab = "intensity", xlab = "wavelength [nm]")
grid()
lambda_led <- data_list[[number]]$wavelength[which(data_list[[number]]$val == max(data_list[[number]]$val))]
print(lambda_led)
}
