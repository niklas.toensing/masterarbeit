#--------------------------------
#plot spectrum from file created with hamamatsu micro-spectrometer
#for master's thesis
#Niklas T�nsing
#Jun 2021
#-------------------------------------------

#Directories
mainpath <- "C:/Users/User/Documents/Masterarbeit/masterarbeit/"
datapath <- paste0(mainpath,"data/")
codepath <- paste0(mainpath, "code/")
#-------------------------------------------

#packages
library(stats)
library(tidyverse)
library(readxl)
library(lubridate)
library(tibbletime)
library(dplyr)
library(tidyr)
library(ggplot2)
library(hrbrthemes)
library(colorspace)
library(data.table)
library(purrr)
library(dygraphs)
library(xts)

setwd(mainpath)
#functions
calib <- function(pix){
  a0 = 3.083536887E2
  b1 = 2.712914405
  b2 = -1.259118844E-3
  b3 = -7.122532621E-6
  b4 = 7.445401152E-9
  b5 = 7.777621893E-12
  return (a0+b1*pix+b2*pix^2+b3*pix^3+b4*pix^4+b5*pix^5)
}

calib1 <- function(data_frame){
  a0 = 3.083536887E2
  b1 = 2.712914405
  b2 = -1.259118844E-3
  b3 = -7.122532621E-6
  b4 = 7.445401152E-9
  b5 = 7.777621893E-12
  return(data_frame$wavelength <- (a0+b1*data_frame$ind+b2*data_frame$ind^2+b3*data_frame$ind^3+b4*data_frame$ind^4+b5*data_frame$ind^5))
}
read_data <- function(filename){
  return (read_delim(filename, delim = ";", col_types = c(col_double(), col_double(), col_character())))
}

#-----------------------------
#main

filenames <- list.files(path = datapath, pattern = "*.txt")
filenames <- paste0(datapath, filenames)

data_frame_list <- lapply(filenames, function(filenames)
  read_delim(filenames, delim = ";", col_types = c(col_double(), col_double(), col_character())))

data_frame_list <-lapply(data_frame_list, function(data_frame_list)
  cbind(data_frame_list, wavelength = calib(data_frame_list$ind)))

data_frame_list <-lapply(data_frame_list, function(data_frame_list)
  transform(data_frame_list, datetime =as.character(datetime)))

data_avg_list <- map(data_frame_list, ~.x %>%
      group_by(wavelength) %>%
      summarise_at(vars(val), funs(mean(., na.rm=TRUE))))


diw_delay100_led0 <- data_avg_list[[which(filenames == 
                                            "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay100_led0.txt")]]
diw_delay200_led0 <- data_avg_list[[which(filenames == 
                                             "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay200_led0.txt")]]
diw_delay300_led0 <- data_avg_list[[which(filenames == 
                                             "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay300_led0.txt")]]
diw_delay500_led0 <- data_avg_list[[which(filenames == 
                                             "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay500_led0.txt")]]
diw_delay1000_led0 <- data_avg_list[[which(filenames == 
                                              "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay1000_led0.txt")]]
diw_delay2000_led0 <- data_avg_list[[which(filenames == 
                                              "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay2000_led0.txt")]]


drop1_delay100_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop1_delay100_led255.txt")]]
drop1_delay200_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop1_delay200_led255.txt")]]

drop1_delay300_led255 <- data_avg_list[[which(filenames == 
                                               "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop1_delay300_led255.txt")]]

drop1_delay500_led255 <- data_avg_list[[which(filenames == 
                                                  "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop1_delay500_led255.txt")]]
drop1_delay1000_led255 <- data_avg_list[[which(filenames == 

###########################################                                                
                                                                                                    "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop1_delay1000_led255.txt")]]
drop2_delay100_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop2_delay100_led255.txt")]]
drop2_delay200_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop2_delay200_led255.txt")]]

drop2_delay300_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop2_delay300_led255.txt")]]

drop2_delay500_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop2_delay500_led255.txt")]]
##############################################################

drop3_delay100_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop3_delay100_led255.txt")]]
drop3_delay200_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop3_delay200_led255.txt")]]

drop3_delay300_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/drop3_delay300_led255.txt")]]
#######################################################

diw_delay2_led255 <- data_avg_list[[which(filenames == 
                                              "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay2_led255.txt")]]
diw_delay20_led255 <- data_avg_list[[which(filenames == 
                                               "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay20_led255.txt")]]
diw_delay50_led255 <- data_avg_list[[which(filenames == 
                                               "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay50_led255.txt")]]
diw_delay100_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay100_led255.txt")]]
diw_delay200_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay200_led255.txt")]]
diw_delay300_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay300_led255.txt")]]
diw_delay500_led255 <- data_avg_list[[which(filenames == 
                                                "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay500_led255.txt")]]
diw_delay1000_led255 <- data_avg_list[[which(filenames == 
                                                 "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay1000_led255.txt")]]
diw_delay2000_led255 <- data_avg_list[[which(filenames == 
                                                 "C:/Users/User/Documents/Masterarbeit/masterarbeit/data/diw_delay2000_led255.txt")]]

diw_delay100_led255$cor <- diw_delay100_led255$val-diw_delay100_led0$val
diw_delay200_led255$cor <- diw_delay200_led255$val-diw_delay200_led0$val
diw_delay300_led255$cor <- diw_delay300_led255$val-diw_delay300_led0$val
diw_delay500_led255$cor <- diw_delay500_led255$val-diw_delay500_led0$val
diw_delay1000_led255$cor <- diw_delay1000_led255$val-diw_delay1000_led0$val
diw_delay2000_led255$cor <- diw_delay2000_led255$val-diw_delay2000_led0$val

drop1_delay100_led255$cor <- drop1_delay100_led255$val-diw_delay100_led0$val
drop1_delay200_led255$cor <- drop1_delay200_led255$val-diw_delay200_led0$val
drop1_delay300_led255$cor <- drop1_delay300_led255$val-diw_delay300_led0$val
drop1_delay500_led255$cor <- drop1_delay500_led255$val-diw_delay500_led0$val
drop1_delay1000_led255$cor <- drop1_delay1000_led255$val-diw_delay1000_led0$val

####################################

drop2_delay100_led255$cor <- drop2_delay100_led255$val-diw_delay100_led0$val
drop2_delay200_led255$cor <- drop2_delay200_led255$val-diw_delay200_led0$val
drop2_delay300_led255$cor <- drop2_delay300_led255$val-diw_delay300_led0$val
drop2_delay500_led255$cor <- drop2_delay500_led255$val-diw_delay500_led0$val

######################################################

drop3_delay100_led255$cor <- drop3_delay100_led255$val-diw_delay100_led0$val
drop3_delay200_led255$cor <- drop3_delay200_led255$val-diw_delay200_led0$val
drop3_delay300_led255$cor <- drop3_delay300_led255$val-diw_delay300_led0$val

#############################################
lambda_raman <- 427.2680
raman_delay100 <- diw_delay100_led255$cor[which((diw_delay100_led255$wavelength - lambda_raman)^2 < 0.1)]
raman_delay200 <- diw_delay200_led255$cor[which((diw_delay200_led255$wavelength - lambda_raman)^2 < 0.1)]
raman_delay300 <- diw_delay300_led255$cor[which((diw_delay300_led255$wavelength - lambda_raman)^2 < 0.1)]
raman_delay500 <- diw_delay500_led255$cor[which((diw_delay500_led255$wavelength - lambda_raman)^2 < 0.1)]
raman_delay1000 <- diw_delay1000_led255$cor[which((diw_delay1000_led255$wavelength - lambda_raman)^2 < 0.1)]
raman_delay2000 <- diw_delay2000_led255$cor[which((diw_delay2000_led255$wavelength - lambda_raman)^2 < 0.1)]

#raman_delay100 <- diw_delay100_led255$cor[[43]]+diw_delay100_led255$cor[[44]]+diw_delay100_led255$cor[[45]]+
 #                 diw_delay100_led255$cor[[46]]+diw_delay100_led255$cor[[47]]+diw_delay100_led255$cor[[48]]+diw_delay100_led255$cor[[49]]
#raman_delay200 <- diw_delay200_led255$cor[[43]]+diw_delay200_led255$cor[[44]]+diw_delay200_led255$cor[[45]]+
#                  diw_delay200_led255$cor[[46]]+diw_delay200_led255$cor[[47]]+diw_delay200_led255$cor[[48]]+diw_delay200_led255$cor[[49]]
#raman_delay300 <- diw_delay300_led255$cor[[43]]+diw_delay300_led255$cor[[44]]+diw_delay300_led255$cor[[45]]+
#                  diw_delay300_led255$cor[[46]]+diw_delay300_led255$cor[[47]]+diw_delay300_led255$cor[[48]]+diw_delay300_led255$cor[[49]]
#raman_delay500 <- diw_delay500_led255$cor[[43]]+diw_delay500_led255$cor[[43]]+diw_delay500_led255$cor[[44]]+diw_delay500_led255$cor[[45]]+
#                  diw_delay500_led255$cor[[46]]+diw_delay500_led255$cor[[47]]+diw_delay500_led255$cor[[48]]+diw_delay500_led255$cor[[49]]
#raman_delay1000 <- diw_delay1000_led255$cor[[43]]+diw_delay1000_led255$cor[[44]]+diw_delay1000_led255$cor[[45]]+
#                    diw_delay1000_led255$cor[[46]]+diw_delay1000_led255$cor[[47]]+diw_delay1000_led255$cor[[48]]+diw_delay1000_led255$cor[[49]]
#raman_delay2000 <- diw_delay2000_led255$cor[[43]]+diw_delay2000_led255$cor[[44]]+diw_delay2000_led255$cor[[45]]+
#                    diw_delay2000_led255$cor[[46]]+diw_delay2000_led255$cor[[47]]+diw_delay2000_led255$cor[[48]]+diw_delay2000_led255$cor[[49]]

drop1_delay100_led255$raman <- drop1_delay100_led255$cor/raman_delay100
drop1_delay200_led255$raman <- drop1_delay200_led255$cor/raman_delay200
drop1_delay300_led255$raman <- drop1_delay300_led255$cor/raman_delay300
drop1_delay500_led255$raman <- drop1_delay500_led255$cor/raman_delay500
drop1_delay1000_led255$raman <- drop1_delay1000_led255$cor/raman_delay1000

################################################

drop2_delay100_led255$raman <- drop2_delay100_led255$cor/raman_delay100
drop2_delay200_led255$raman <- drop2_delay200_led255$cor/raman_delay200
drop2_delay300_led255$raman <- drop2_delay300_led255$cor/raman_delay300
drop2_delay500_led255$raman <- drop2_delay500_led255$cor/raman_delay500

####################################################

drop3_delay100_led255$raman <- drop3_delay100_led255$cor/raman_delay100
drop3_delay200_led255$raman <- drop3_delay200_led255$cor/raman_delay200
drop3_delay300_led255$raman <- drop3_delay300_led255$cor/raman_delay300
drop3_delay500_led255$raman <- drop3_delay500_led255$cor/raman_delay500

############plotting#####################

plot_colors <- c("#E16A86",
                 "#C18500",
                 "#799D00",
                 "#00AB6E",
                 "#00A9BE",
                 "#6C8EE6")
######diw
plot(diw_delay100_led255$wavelength, diw_delay100_led255$cor, main = "diw_led255", "l",
     ylab = "intensity [AU]", xlab = "wavelength [nm]", xlim = c(350,600), col = plot_colors[[1]], lwd = 1.5)
lines(diw_delay200_led255$wavelength, diw_delay200_led255$cor, col = plot_colors[[2]], "l", lwd = 1.5)
lines(diw_delay300_led255$wavelength, diw_delay300_led255$cor, col = plot_colors[[3]], "l", lwd = 1.5)
lines(diw_delay500_led255$wavelength, diw_delay500_led255$cor, col = plot_colors[[4]], "l", lwd = 1.5)
lines(diw_delay1000_led255$wavelength, diw_delay1000_led255$cor, col = plot_colors[[5]], "l", lwd = 1.5)
lines(diw_delay2000_led255$wavelength, diw_delay2000_led255$cor, col = plot_colors[[6]], "l", lwd = 1.5)
abline(v = seq(350,600,10),  lty = "dotted", col = "grey")
legend("topright", legend = c("delay100","delay200","delay300","delay500", "delay1000", "delay2000"), col = plot_colors,
       lty = "solid", lwd = 1.5)

###########drop1_raman
plot(drop1_delay100_led255$wavelength, drop1_delay100_led255$raman, main = "drop1 raman", "l",
     ylab = "intensity [raman units]", xlab = "wavelength [nm]", xlim = c(350,600), ylim = c(0,2), col = plot_colors[[1]], lwd = 1.5)
lines(drop1_delay200_led255$wavelength, drop1_delay200_led255$raman, col = plot_colors[[2]], lwd = 1.5)
lines(drop1_delay300_led255$wavelength, drop1_delay300_led255$raman, col = plot_colors[[3]], lwd = 1.5)
lines(drop1_delay500_led255$wavelength, drop1_delay500_led255$raman, col = plot_colors[[4]], lwd = 1.5)
lines(drop1_delay1000_led255$wavelength, drop1_delay1000_led255$raman, col = plot_colors[[5]], lwd = 1.5)
abline(v = seq(350,600,10),  lty = "dotted", col = "grey")
legend("topright", legend = c("delay100","delay200","delay300","delay500", "delay1000"), col = plot_colors,
       lty = "solid", lwd = 1.5)


############drop1_raw
plot(drop1_delay100_led255$wavelength, drop1_delay100_led255$cor, main = "drop1 raw", "l",
     ylab = "intensity", xlab = "wavelength [nm]", xlim = c(350,600), col = plot_colors[[1]], lwd = 1.5)
lines(drop1_delay200_led255$wavelength, drop1_delay200_led255$cor, col = plot_colors[[2]], lwd = 1.5)
lines(drop1_delay300_led255$wavelength, drop1_delay300_led255$cor,  col = plot_colors[[3]], lwd = 1.5)
lines(drop1_delay500_led255$wavelength, drop1_delay500_led255$cor,  col = plot_colors[[4]], lwd = 1.5)
lines(drop1_delay1000_led255$wavelength, drop1_delay1000_led255$cor,  col = plot_colors[[5]], lwd = 1.5)
abline(v = seq(350,600,10),  lty = "dotted", col = "grey")
legend("topright", legend = c("delay100","delay200","delay300","delay500", "delay1000"), col = plot_colors,
       lty = "solid", lwd = 1.5)

##########comparison_diw_drop1
plot(drop1_delay500_led255$wavelength, drop1_delay500_led255$cor, main = "drop1 raw", "l",
     ylab = "intensity", xlab = "wavelength [nm]", xlim = c(350,600), col = plot_colors[[1]], lwd = 2)
lines(diw_delay500_led255$wavelength, diw_delay500_led255$cor, col = plot_colors[[3]], lwd = 2)
abline(v = seq(350,600,10),  lty = "dotted", col = "grey")
legend("topright", legend = c("drop1","diw"), col = c(plot_colors[[1]],plot_colors[[3]]),
       lty = "solid", lwd = 2)


#############comp drop 1 drop 2 raman
plot(drop1_delay100_led255$wavelength, drop1_delay100_led255$raman, main = "drop1 drop2 drop3 raman", "l",
     ylab = "intensity [raman units]", xlab = "wavelength [nm]", xlim = c(350,600), ylim = c(0,20), col = plot_colors[[1]], lwd = 1.5)
lines(drop1_delay200_led255$wavelength, drop1_delay200_led255$raman, col = plot_colors[[2]], lwd = 1.5)
lines(drop1_delay300_led255$wavelength, drop1_delay300_led255$raman, col = plot_colors[[3]], lwd = 1.5)
lines(drop1_delay500_led255$wavelength, drop1_delay500_led255$raman, col = plot_colors[[4]], lwd = 1.5)

lines(drop2_delay100_led255$wavelength, drop2_delay100_led255$raman, col = plot_colors[[1]], lwd = 1.5, lty = "dashed")
lines(drop2_delay200_led255$wavelength, drop2_delay200_led255$raman, col = plot_colors[[2]], lwd = 1.5, lty = "dashed")
lines(drop2_delay300_led255$wavelength, drop2_delay300_led255$raman, col = plot_colors[[3]], lwd = 1.5, lty = "dashed")
lines(drop2_delay500_led255$wavelength, drop2_delay500_led255$raman, col = plot_colors[[4]], lwd = 1.5, lty = "dashed")

lines(drop3_delay100_led255$wavelength, drop3_delay100_led255$raman, col = plot_colors[[1]], lwd = 1.5, lty = "dotted")
lines(drop3_delay200_led255$wavelength, drop3_delay200_led255$raman, col = plot_colors[[2]], lwd = 1.5, lty = "dotted")
lines(drop3_delay300_led255$wavelength, drop3_delay300_led255$raman, col = plot_colors[[3]], lwd = 1.5, lty = "dotted")
abline(v = seq(350,600,10),  lty = "dotted", col = "grey")
legend("topright", legend = c("delay100","delay200","delay300","delay500"), col = plot_colors,
       lty = "solid", lwd = 1.5)


dygraph(data_avg5, main = "mean")  %>% 
  dyAxis("y", label = "intensity", independentTicks = T) %>%
  dySeries("cor_val", axis = "y",fillGraph = T, color = "red") %>%
  dyRangeSelector()   %>%   
  dyOptions(stepPlot = T, connectSeparatedPoints = T, 
            fillGraph = T, fillAlpha = 0.2)

for (i in (1:length(data_list))){
number <- i
plot(data_list[[number]]$wavelength, data_list[[number]]$val, main = data_list[[number]]$datetime[1], "l",
     ylab = "intensity", xlab = "wavelength [nm]")
grid()
lambda_led <- data_list[[number]]$wavelength[which(data_list[[number]]$val == max(data_list[[number]]$val))]
print(lambda_led)
}
