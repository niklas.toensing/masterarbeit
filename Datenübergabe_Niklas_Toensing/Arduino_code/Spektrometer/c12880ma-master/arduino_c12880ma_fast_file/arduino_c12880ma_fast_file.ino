#include <TimeLib.h> //library for time functions
#include <DS1307RTC.h> //library to read rtc
#include <Wire.h>

 /*
 * Macro Definitions
 */
#define SPEC_TRG         A0
#define SPEC_ST          4
#define SPEC_CLK         3
#define SPEC_VIDEO       A3
#define WHITE_LED        5
#define LASER_404        A5
#include <elapsedMillis.h>

#define SPEC_CHANNELS    200 // New Spec Channel
uint16_t data[SPEC_CHANNELS];

tmElements_t tm;
char timest[15]; //19 digits plus the null char

const int buttonPin = 2;     // the number of the pushbutton pin
int buttonState = 0;         // variable for reading the pushbutton status
//buttonmemory
int buttonmemo = 0;


void setup(){

  //Set desired pins to OUTPUT
  pinMode(SPEC_CLK, OUTPUT);
  pinMode(SPEC_ST, OUTPUT);
  pinMode(LASER_404, OUTPUT);
  pinMode(WHITE_LED, OUTPUT);

  digitalWrite(SPEC_CLK, HIGH); // Set SPEC_CLK High
  digitalWrite(SPEC_ST, LOW); // Set SPEC_ST Low
  // initialize the spec clock with fastest period
//    spec.setClockPeriod(MIN_CLOCK_PULSE_US);
  Serial.begin(115200); // Baud Rate set to 115200
}

/*
 * This functions reads spectrometer data from SPEC_VIDEO
 * Look at the Timing Chart in the Datasheet for more info
 */
void readSpectrometer(){
  int delayTime = 1; // delay time
  // Start clock cycle and set start pulse to signal start
  digitalWrite(SPEC_CLK, LOW);
  
 delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
  digitalWrite(SPEC_ST, HIGH);
     delayMicroseconds(delayTime);
  //Sample for a period of time
  for(int i = 0; i < 15; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
 
  }

  //Set SPEC_ST to low
  digitalWrite(SPEC_ST, LOW);

  //Sample for a period of time
  for(int i = 0; i < 85; i++){

      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime); 
      
  }

  //One more clock pulse before the actual read
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  digitalWrite(SPEC_CLK, LOW);
     delayMicroseconds(delayTime);
  //Read from SPEC_VIDEO
  for(int i = 0; i < SPEC_CHANNELS; i++){
      data[i] = analogRead(SPEC_VIDEO);
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
        
  }
 
  //Set SPEC_ST to high
  digitalWrite(SPEC_ST, HIGH);

  //Sample for a small amount of time
  for(int i = 0; i < 6; i++){
    
      digitalWrite(SPEC_CLK, HIGH);
         delayMicroseconds(delayTime);
      digitalWrite(SPEC_CLK, LOW);
         delayMicroseconds(delayTime);
    
  }

  //Set SPEC_ST to low
  //digitalWrite(SPEC_ST, LOW);
  
  digitalWrite(SPEC_CLK, HIGH);
     delayMicroseconds(delayTime);
  
}

/*
 * The function below prints out data to the terminal or 
 * processing plot
 */
void printData(){
  
  for (int i = 0; i < SPEC_CHANNELS; i++){
    
    Serial.print(data[i]);
    Serial.print(',');
    
  }
  
  Serial.print("\n");
}

char *getTimestamp(){ 
  tmElements_t tm;
  if (RTC.read(tm)) { //read current time
     sprintf(timest, "%02d%02d%4d%02d%02d%02d", tm.Day, tm.Month, tmYearToCalendar(tm.Year), tm.Hour, tm.Minute, tm.Second);
  }
  return timest;
}

void loop(){
  buttonState = digitalRead(buttonPin);
  if (buttonState == 1 and buttonmemo == 0) {
    buttonmemo = 1;
    //Serial.println("button pressed");
  }
  if (buttonState == 0 and buttonmemo == 1){
    //Serial.println("read spectrometer");
    analogWrite(WHITE_LED,255);
    getTimestamp();
    Serial.print(timest);
    Serial.print(";");
    readSpectrometer();
    delay(200);
    readSpectrometer();
    printData();
    buttonmemo = 0;
    delay(10);  
    analogWrite(WHITE_LED,0);
  }
}
